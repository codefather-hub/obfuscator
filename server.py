#!/usr/bin/env python

from pyftpdlib import servers
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.authorizers import DummyAuthorizer

address = ("0.0.0.0", 9000)
server = servers.FTPServer(address, FTPHandler)

authorizer = DummyAuthorizer()
authorizer.add_user("admin", "admin", "workspace", perm="elradfmwMT")

FTPHandler.authorizer = authorizer

server.serve_forever()