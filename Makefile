sleep:
	python3 -c "import time; time.sleep(3)"
	
# step 1
build:
	docker-compose build --no-cache

# step 2
idle:
	docker-compose run core bash

# step 3
sdk:
	/app/tools/bin/sdkmanager --update
	echo y | /app/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
			 "platforms;android-${ANDROID_VERSION}" "platform-tools"


# все шаги исполнять по очереди. третий щаг исполнять строго после второго, находясь внутри докер контейнера
# Поднятие описано пошагово в Makefile
# Запуск скрипта обфускатора осуществяется внутри докер контейнера core командой
# `docker-compose run core bash` и после этого находясь в контейнере перейти в папку src и запустить скрипт `python -m obfuscapk.cli`.
# Подробности использования обфусктаора смотреть в документации 
