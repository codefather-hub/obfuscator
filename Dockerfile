FROM ubuntu:20.04

MAINTAINER @codefather-labs

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update
RUN apt-get install gcc g++ cmake openjdk-11-jdk python3 python3-dev python3-pip wget curl unzip -y

COPY ./* /app/
WORKDIR /app/

RUN python3 -m pip install --upgrade pip 
RUN python3 -m pip install -r /app/requirements.txt

USER root
EXPOSE 20 21

COPY ./apktool.jar /usr/local/bin/apktool.jar
COPY ./apktool.sh /usr/local/bin/apktool.sh

RUN chmod +x /usr/local/bin/apktool.jar
RUN chmod +x /usr/local/bin/apktool.sh

# RUN curl -o /app/tools.zip https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip
# RUN unzip /app/tools.zip && rm -rf /app/tools.zip

ENV ANDROID_HOME="/app" \
    ANDROID_SDK_ROOT="/app" \
    ANDROID_TOOLS="/app/tools" \
    ANDROID_PLATFORM_TOOLS="/app/platform-tools" \
    ANDROID_BUILD_TOOLS="/app/build-tools" \
    ANDROID_VERSION=28 \
    ANDROID_BUILD_TOOLS_VERSION=28.0.3 \
    APKTOOL_PATH=/usr/local/bin/apktool.jar \
    JARSIGNER_PATH=/usr/bin/jarsigner \
    ZIPALIGN_PATH="/app/build-tools/zipalign" \
    android="/app/tools/android" \
    sdkmanager="/app/tools/bin/sdkmanager" \
    avdmanager="/app/tools/bin/avdmanager" \
    
# RUN /app/tools/bin/sdkmanager --update
# RUN echo y | /app/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
#     "platforms;android-${ANDROID_VERSION}" \
#     "platform-tools"